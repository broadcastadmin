<?php
  define ('OWNER', 'ZZBO');
  define ('ABSOLUTEDIR', '/var/www/bba/htdocs');
  define ('TEMPLATEDIR', ABSOLUTEDIR.'/xsl');
  define ('PREVIEWDIR', ABSOLUTEDIR.'/preview');
  define ('USER_IMAGEDIR', ABSOLUTEDIR.'/fotos');
  define ('BROADCASTDIR', ABSOLUTEDIR.'/broadcast');
  define ('BROADCASTCACHEDIR', ABSOLUTEDIR.'/broadcast/cache');
  define ('CACHEDIR', '/tmp/broadcast');
  define ('DATABASE', ABSOLUTEDIR.'/database/'.strtolower(OWNER).'-parkiet.db');
  define ('REMOTEHOST', '127.0.0.1');
  define ('REMOTEDIR', '/home/tv/broadcast');
  define ('RESOLUTIONW', '1024');
  define ('RESOLUTIONH', '768');
  define ('EMERGENCY', false);
?>
