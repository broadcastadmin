<?php
define('YES', 'Yes');
define('NO', 'No');
define('NORESULT', 'No results found');
define('WELCOME', 'Welcome at the new version of BulletinBoardAdmin.');
define('EMERG', 'EMERGENCY BROADCASTING ENABLED');
define('GENERATE', 'Generate = Copy');
define('ADD', 'Add');
define('NEXTPER', 'Next period');
define('START', 'start');
define('UNTIL', 'until');
define('LENGTH', 'length');
define('LASTMOD', 'last modification by');
define('VIEW', 'view');
define('UNDEFINED', 'undefined');


define('EXAMPLE', 'Example');
define('PLAYEDAT', 'Played At');
define('ACTION', 'Actions');
define('ACT_UPD', 'Update');
define('ACT_ADD', 'Add');
define('ACT_DEL', 'Delete');
define('ACT_NEW', 'New');
define('ACT_SAVE', 'Save');
define('ACT_NEWI', 'New Item');
define('ACT_NEWP', 'New Playtime');
define('ACT_BACK', 'Back');

define('TEXTPAGE', 'Text Page');
define('TEMPLATE', 'Template');
define('PHOTO', 'Photo');
define('TITLE', 'Title');
define('TEXT', 'Text');
define('NEWIMAGE', 'New Image');
define('UPLOAD', 'Upload');

define('NEWTEMP', 'New Template');
define('CATEGORY', 'Category');
define('PARAM', 'Parameters');
define('LEFT', 'Parameters');
define('TOP', 'Parameters');
define('HEIGHT', 'Parameters');
define('WIDTH', 'Parameters');

define('TODAY', 'Today');

?>
